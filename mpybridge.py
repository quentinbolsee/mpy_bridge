# Quentin Bolsée 2023-10-01
# MIT Center for Bits and Atoms
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
import os.path
import serial
import serial.tools.list_ports
import time
import re
import ast


class MicroPythonError(RuntimeError):
    pass


class MicroPythonFileError(MicroPythonError):
    pass


class Device:
    init_delay = 0.15
    timeout_delay = 2.0

    def __init__(self, port, main_filename="main.py", show_prints=True, init=True):
        self.ser_obj = serial.Serial(port, timeout=self.timeout_delay)
        self.function_args = {}
        self.function_callable = {}
        self.main_filename = main_filename
        self.main_module = main_filename.replace(".py", "")
        self.show_prints = show_prints
        self.reset()
        if init:
            self.init()

    def reset(self):
        s = self.ser_obj
        s.write(b'\x04')
        s.write(b'\r\n\x03\r\n')
        s.write(b'\x01')
        time.sleep(self.init_delay)

        s.flushInput()
        # wipe out previous state
        for name in self.function_args:
            delattr(self, name)
        self.function_args = {}
        self.function_callable = {}

    def init(self):
        # read functions
        self.read_functions()
        # make sure main file is imported
        self.run(f'import {self.main_module}')

    def run(self, cmd, end="\n", ignore_error=False):
        s = self.ser_obj
        s.write((cmd+end).encode("utf-8"))
        s.write(b'\x04')  # ^d reset

        # OK<RETURN>\x04
        txt_ret = s.read_until(b"\x04")[2:-1].decode("utf-8")

        # <ERROR>\x04>
        txt_err = s.read_until(b"\x04>")[:-2].decode("utf-8")

        if len(txt_err) > 0 and not ignore_error:
            raise MicroPythonError(txt_err)

        return txt_ret.rstrip()

    def run_func(self, func_name, *args, **kwargs):
        args_list = list(repr(x) for x in args)
        kwargs_list = list(f"{a}={repr(b)}" for a, b in kwargs.items())
        cmd_txt = f"_ret = {self.main_module}.{func_name}({','.join(args_list+kwargs_list)})"
        ret_print = self.run(cmd_txt)
        if self.show_prints and len(ret_print) > 0:
            print(f"MPY_PRINT@{self.ser_obj.port}:{ret_print}")
        ret_val = self.run(f"print(repr(_ret))")
        return ast.literal_eval(ret_val)

    def read_functions(self):
        # open file
        try:
            self.run(f'f=open("{self.main_filename}","rb")')
        except MicroPythonError as e:
            raise MicroPythonFileError(str(e))

        # read main txt file
        main_txt = ast.literal_eval(self.run('print(f.read())')).decode("utf-8")

        # find all functions
        matches = re.finditer(r"def\s+([^(]+)\((.*)\):", main_txt)
        for m in matches:
            name = m.group(1)
            # generate function
            func = lambda *args, func_name=name, **kwargs: self.run_func(func_name, *args, **kwargs)
            self.function_args[name] = m.group(2)
            self.function_callable[name] = func
            setattr(self, name, func)

    def testREPL(self):
        # reduce waiting time for this simple test
        txt = self.run("print(6*7)")
        if len(txt) == 0:
            return False
        return ast.literal_eval(txt) == 42

    def __str__(self):
        txt = f"MicroPython Device at {self.ser_obj.port}, available functions:\n"
        if len(self.function_args) == 0:
            txt += "  none\n"
        else:
            for a, b in self.function_args.items():
                txt += f"  {a}({b})\n"
        return txt[:-1]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def update(self):
        self.init()

    def upload_main(self, filename):
        self.upload(filename, self.main_filename)

    def upload(self, filename, destination=None, init=True):
        if destination is None:
            _, destination = os.path.split(filename)
        with open(filename, "r") as f:
            file_txt = f.read()
        self.run(f'f = open("{destination}", "wb")')
        self.run(f'f.write({repr(file_txt)})')
        self.run(f'f.close()')
        self.reset()
        if init:
            self.init()

    def remove(self, filename):
        self.run('import os')
        self.run(f'os.remove("{filename}")')

    def close(self):
        self.ser_obj.close()
