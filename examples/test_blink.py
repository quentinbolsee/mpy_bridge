import mpybridge
import time


def main():
    # open device, read functions
    d = mpybridge.Device("COM39")

    # print out functions
    print(d)

    # # blink LED once
    d.led_on()
    time.sleep(0.2)
    d.led_off()
    time.sleep(0.2)

    print(d.echo_function(4, 5, a=10))


if __name__ == "__main__":
    main()
