import time
import machine


def get_pin_value(pin):
    return machine.Pin(pin).value()


def echo_function(*args, **kwargs):
    return "You sent args {}, kwargs {}".format(args, kwargs)


def led_on():
    print("LED ON")
    p_led.off()


def led_off():
    print("LED OFF")
    p_led.on()


p_led = machine.Pin(16, machine.Pin.OUT)
led_off()
