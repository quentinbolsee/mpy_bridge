import mpybridge


def main():
    # Open device, skip reading its functions (the main file might be absent)
    d = mpybridge.Device("COM39", init=False)
    # Upload main file
    d.upload("micropython/main.py", "main.py")


if __name__ == "__main__":
    main()
