import datetime
import mpybridge
import numpy as np
import matplotlib.pyplot as plt


def main():
    d = mpybridge.Device("COM39")
    n_exp = 1024
    results = []

    for i in range(n_exp):
        t1 = datetime.datetime.now()
        d.get_pin_value(4)
        t2 = datetime.datetime.now()
        results.append((t2 - t1).total_seconds())

    print(f"Mean RTT: {np.mean(results)}")
    print(f"STD RTT: {np.std(results)}")

    results_ms = [1000*x for x in results]

    plt.figure()
    plt.ylabel("count")
    plt.xlabel("Round-trip time (ms)")
    plt.title("Round-trip time histogram, 1024 samples")
    plt.hist(results_ms, 64)
    plt.show()


if __name__ == "__main__":
    main()
